#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/i_habits_tracker.h"

#include "../../Interfaces/Architecture/GUIElementBase/guielementbase.h"
class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.HabitsTracker" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	GUIElementBase* m_GUIElementBase;
	ReferenceInstancePtr<IHabitsTracker> m_habitsTracker;
};
