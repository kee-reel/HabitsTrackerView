#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
    , m_GUIElementBase(new GUIElementBase(this, {"MainMenuItem"}, "qrc:/HabitsTrackerView.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IGUIElement), m_GUIElementBase}
	},
	{
		{INTERFACE(IHabitsTracker), m_habitsTracker}
	},
	{}
	);
	m_GUIElementBase->initGUIElementBase();
}

Plugin::~Plugin()
{
}
