import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    Component {
        id: eventListHeader

        Row {
            id: eventDateRow
            width: parent.width
            height: eventDayLabel.height
            spacing: 10 * ratio

            Label {
                id: eventDayLabel
                text: habitsTrackerObject.currentDate.getDate()
                font.pointSize: 35 * ratio
                color: systemPalette.text
            }

            Label {
                readonly property var options: {
                    weekday: "long"
                }
                text: Qt.locale().standaloneDayName(
                          habitsTrackerObject.currentDate.getDay(),
                          Locale.LongFormat)
                font.pointSize: 18 * ratio
                color: systemPalette.text
            }
            Label {
                text: Qt.locale().standaloneMonthName(
                          habitsTrackerObject.currentDate.getMonth(
                              )) + habitsTrackerObject.currentDate.toLocaleDateString(
                          Qt.locale(), " yyyy")
                font.pointSize: 18 * ratio
                color: systemPalette.text
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: systemPalette.window
        border.color: "#d8dee9"

        ListModel {
            id: emptyModel
        }

        ListView {
            id: eventsListView
            spacing: 4 * ratio
            clip: true
            header: eventListHeader
            anchors.fill: parent
            anchors.margins: 5 * ratio
            model: habitsTrackerObject !== null
                   && habitsTrackerObject !== undefined ? habitsTrackerObject.habits : emptyModel

            delegate: Rectangle {
                width: eventsListView.width
                height: nameLabel.height * 1.3
                color: systemPalette.window
                border.color: "#d8dee9"

                Label {
                    id: nameLabel
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    height: 50 * ratio
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: modelData.name
                    color: systemPalette.text
                }
                Button {
                    id: removeButton
                    anchors.right: streakLabel.left
                    anchors.rightMargin: 10 * ratio
                    icon.source: "qrc:/res/ic_delete_black_24dp.png"
                    height: 50 * ratio
                    width: height
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: modelData.remove()
                }
                Label {
                    id: streakLabel
                    anchors.right: streakNumber.left
                    anchors.rightMargin: 10 * ratio
                    text: "Current streak:"
                    font.pixelSize: 18 * ratio
                    color: systemPalette.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                Rectangle {
                    id: streakNumber
                    width: 50 * ratio
                    anchors.verticalCenter: parent.verticalCenter
                    height: width
                    color: "#d8dee9"
                    radius: 90
                    anchors.right: completeButton.left
                    anchors.rightMargin: 10 * ratio
                    Rectangle {
                        anchors.fill: parent
                        opacity: modelData.streak >= 21 ? 1 : modelData.streak / 21.0
                        color: "gold"
                        radius: 90
                    }
                    Label {
                        anchors.fill: parent
                        text: modelData.streak
                        color: systemPalette.window
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
                Button {
                    id: completeButton
                    anchors.right: parent.right
                    anchors.rightMargin: 10 * ratio
                    icon.source: "qrc:/res/ic_done_black_24dp.png"
                    enabled: !modelData.isCompleted
                    height: 50 * ratio
                    width: height
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: modelData.complete()
                }
            }
        }
    }
}
